LOCALHOST = "http://127.0.0.1:5000"
SERVER_PATH = "https://pv080.fi.muni.cz"

SERVER_MESSAGE_PATH = f"{SERVER_PATH}/msg/message"
SERVER_KEY_PATH = f"{SERVER_PATH}/msg/key"
SERVER_CERT_REQUEST_CHALLENGE = f"{SERVER_PATH}/cr/certificates/request-challenge"
SERVER_CERT_VERIFY_CHALLENGE = f"{SERVER_PATH}/cr/certificates/verify-challenge"
SERVER_CERTIFICATES = f"{SERVER_PATH}/cr/certificates"
