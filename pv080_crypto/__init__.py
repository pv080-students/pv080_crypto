"""
The ``pv080_crypto`` package contains functions that will be used during
cryptography-related seminars in the course PV080 Information Security and
Cryptography.

The functionality is split into multiple modules:

- :py:mod:`messaging <pv080_crypto.messaging>` contains functions for
  communicating via the `PV080 messaging service
  <https://pv080.fi.muni.cz/msg>`_;

- :py:mod:`symmetric <pv080_crypto.symmetric>` deals with symmetric
  encryption (mostly using AES), hashing and MACs;

- :py:mod:`asymmetric <pv080_crypto.asymmetric>` deals with asymmetric
  encryption and digital signatures (using RSA);

- :py:mod:`certificates <pv080_crypto.certificates>` deals with public key
  certificates;

- :py:mod:`utils <pv080_crypto.utils>` contains some utility functions.

The ``pv080_crypto`` package is built on top of the Python `cryptography
<https://cryptography.io/en/latest/>`_ package.
"""

# FIXME Format the link to Pyca cryptography nicer.

from pv080_crypto.messaging import send_message, recv_message
from pv080_crypto.symmetric import (
    pad,
    unpad,
    aes_ecb_encrypt,
    aes_ecb_decrypt,
    aes_cbc_encrypt,
    aes_cbc_decrypt,
    XOR,
    chacha20_encrypt,
    chacha20_decrypt,
    aes_encrypt,
    aes_decrypt,
    sha256_hash,
    create_mac,
    verify_mac,
)
from pv080_crypto.asymmetric import (
    publish_key,
    fetch_key,
    rsa_encrypt,
    rsa_decrypt,
    create_signature,
    verify_signature,
)
from pv080_crypto.utils import (
    store_private_key,
    load_private_key,
    store_cert,
    load_cert,
    extract_names,
    create_csr,
)
from pv080_crypto.certificates import (
    verify_cert_signature,
    request_cert,
    verify_challenge,
    fetch_cert,
)

__all__ = [
    # messaging
    "send_message",
    "recv_message",
    # symmetric
    "pad",
    "unpad",
    "aes_ecb_encrypt",
    "aes_ecb_decrypt",
    "aes_cbc_encrypt",
    "aes_cbc_decrypt",
    "XOR",
    "chacha20_encrypt",
    "chacha20_decrypt",
    "aes_encrypt",
    "aes_decrypt",
    "sha256_hash",
    "create_mac",
    "verify_mac",
    # asymmetric
    "publish_key",
    "fetch_key",
    "rsa_encrypt",
    "rsa_decrypt",
    "create_signature",
    "verify_signature",
    # certificates
    "verify_cert_signature",
    "request_cert",
    "verify_challenge",
    "fetch_cert",
    # utils
    "store_private_key",
    "load_private_key",
    "store_cert",
    "load_cert",
    "extract_names",
    "create_csr",
]
