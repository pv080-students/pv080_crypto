{
  description = "PV080 Cryptography Package";

  inputs = {
    nixpkgs.url      = "github:NixOS/nixpkgs";
    flake-utils.url  = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ ];
        pkgs = nixpkgs.legacyPackages.${system};
        pythonPackages = with pkgs.python311Packages; [
          ipython
          mypy
          black
          venvShellHook
        ];
      in
      with pkgs;
      {
        devShells.default = mkShell {

          buildInputs = [
            python311
          ] ++ pythonPackages;

          venvDir = "venv";
          postVenvCreation = ''
            unset SOURCE_DATE_EPOCH
            pip install --upgrade pip
            pip install wheel

            pip install --editable ".[tests,docs]"
          '';
        };
      }
    );
}
