# Checklist

- [ ] Increment the version in `pyproject.toml`, [Semantic Versoning](https://semver.org/) can be utilized (semi-rigorously, no need to go bonkers).
- [ ] After merging, push the `main` into the `main` of [PV080 students](https://gitlab.fi.muni.cz/pv080-students/pv080_crypto/).
- [ ] After merging do not forget to ping @x408788 to update `I:\pv080\seminars\venv` virtual.
