.. PV080 Crypto documentation master file, created by
   sphinx-quickstart on Wed Mar  1 10:52:57 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PV080 Crypto's documentation!
========================================

.. automodule:: pv080_crypto
   :no-members:

.. toctree::
   :maxdepth: 1
   :caption: Individual modules:
   :hidden:

   api/messaging.rst
   api/symmetric.rst
   api/asymmetric.rst
   api/certificates.rst
   api/utils.rst

.. toctree::
    :maxdepth: 1
    :caption: External links:

    Assignment web <https://pv080.fi.muni.cz/>
    Course homepage <https://is.muni.cz/auth/predmet/fi/PV080>
    GitLab PV080 Crypto repository <https://gitlab.fi.muni.cz/pv080-students/pv080_crypto>

.. warning::
   The purpose of ``pv080_crypto`` is purely educational and it MUST NOT be used as the cryptographic library in a real-project.



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
